exports.helloWorld = (req, res) => {
  let message = req.query.message || req.body.message || 'Hello Aditya!';
  res.status(200).send(message);
};